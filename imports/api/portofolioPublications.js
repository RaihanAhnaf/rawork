import { Meteor } from 'meteor/meteor';
import { PortofolioCollection } from './portofolio'

Meteor.publish('portofolio', function publishPortofolio(id) {
  return PortofolioCollection.find({_id: id});
});

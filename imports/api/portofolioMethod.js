import { PortofolioCollection } from './portofolio';
 
Meteor.methods({
  'portofolio.insert'(img, title, description) {
 
    PortofolioCollection.insert({img, title, description, createdAt: new Date()});
  },

  'portofolio.delete'(id) {
 
    PortofolioCollection.remove(id);
  },

  'portofolio.update'(id, img, title, description) {
 
    PortofolioCollection.update(id, {
        $set: {img, title, description}
    });
  },
});
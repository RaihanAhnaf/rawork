import { TestimoniCollection } from './testimoni';
 
Meteor.methods({
  'testimoni.insert'(name, review) {
 
    TestimoniCollection.insert({name, review, status:false, createdAt: new Date()});
  },

  'testimoni.delete'(id) {
 
    TestimoniCollection.remove(id);
  },

  'testimoni.tampil'(id, status) {
 
    TestimoniCollection.update(id, {
        $set: {status}
    });
  },
});
import React from "react";
import PortoTile from "./components/portofolios/PortoTile";
import { useTracker } from "meteor/react-meteor-data";
import { PortofolioCollection } from "../api/portofolio";

export const Portofolio = () => {
    const portofolio = useTracker(() => PortofolioCollection.find({}).fetch())
    return(
        <div>
        <div className="h_porto">
            <center><h2>Our last Work</h2>
            <h6>This is our Portofolio</h6></center>
            <div className="container">
                <div className="row">
                    {
                        portofolio.map(portofolio => {
                            return <PortoTile
                            portoImg = {portofolio.img}
                            title = {portofolio.title}
                            description = {portofolio.description}
                            />
                        })
                    }
                </div>
            </div>
        </div>
    </div>
    )   
};
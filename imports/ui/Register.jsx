import React from "react";
import { Link } from "react-router-dom";

export const Register = () => {
    return (
        <div>
            <div className="register">
                <nav className="navbar justify-content-between d-flex flex-row">
                    <h3><b>Rawork.</b></h3>
                    <div >
                        <ul>
                            <li><Link to="/">Home</Link></li>
                            <li><Link to="/login">Sign In</Link></li>
                        </ul>
                    </div>
                </nav>
                <div className="container">
                    <center>
                        <h1>Sign Up</h1>
                    </center>
                    <div className="form-control form-control-inline">
                        <form>
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Username</label><br />
                                    <input className="form-control" type="text" />
                                    <br />
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Email</label><br />
                                    <input className="form-control" type="text" />
                                    <br />
                                </div>
                            </div>
                            <div className="form-group">
                                <label>Password</label><br />
                                <input className="form-control" type="password" />
                                <br />
                            </div>
                            <br />
                            <div className="d-grid gap-2 col-2 mx-auto">
                                <button className="btn btn3" type="submit">Submit</button>
                                <br />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}
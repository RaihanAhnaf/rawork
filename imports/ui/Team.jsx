import React from "react";
import TeamTile from "./components/team/TeamTile";

export const Team = () => (
    <div>
        <div className="h_team">
            <center>
                <h2>Our Team</h2>
                <h6>Our Consultant are here</h6>
            </center>
            <div className="container">
                <div className="row">
                    <TeamTile
                    foto = "./foto.png"
                    nama = "Raihan"
                    jabatan = "UI Designer"
                    />
                    <TeamTile
                    foto = "./foto2.png"
                    nama = "Ahnaf"
                    jabatan = "UX Researcher"
                    />
                    <TeamTile
                    foto = "./foto3.png"
                    nama = "Nafisa"
                    jabatan = "Fullstack Dev"
                    />
                    <TeamTile
                    foto = "./foto4.png"
                    nama = "Putra"
                    jabatan = "Mobile Dev"
                    />
                </div>
            </div>
        </div>
    </div>
);
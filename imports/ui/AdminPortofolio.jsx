import React from "react";
import { useTracker } from "meteor/react-meteor-data";
import { PortofolioCollection } from "../api/portofolio";
import { Link } from "react-router-dom";
import { authenticated } from "./store";
import { useRecoilState } from "recoil";

export const AdminPortofolio = () => {
    const [auth, setAuth] = useRecoilState(authenticated);
    const portofolio = useTracker(() => PortofolioCollection.find({}).fetch())
    const deletePortofolio = (id) => Meteor.call("portofolio.delete", id)
    const logout = () => {
        Meteor.logout((err) => {
          if (err) console.log(err);
          else {
            setAuth({ check: false, user: [] });
          }
        });
      };
    return (
        <>
            <div className="admporto">
                <nav className="navbar justify-content-between d-flex flex-row">
                    <h3><b>Rawork.</b></h3>
                    <div >
                        <ul>
                            <li><Link to="/">Home</Link></li>
                            <li><Link to="/portofolio">Portofolio</Link></li>
                            <li><Link to="/testi">Testimoni</Link></li>
                            <li onClick={() => logout()}><Link to="#">Logout</Link></li>
                        </ul>
                    </div>
                </nav>
                <div className="container">
                    <center>
                        <h1>Portofolio</h1>
                    </center>
                    <Link to="/addportofolio" className="btn btn-primary btn-add">Tambah Data</Link>
                    <table className="table table-hover">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                portofolio.map((portofolio, index) => {
                                    return (
                                        <tr key={portofolio._id}>
                                            <td>{index + 1}</td>
                                            <td>{portofolio.title}</td>
                                            <td>{portofolio.description}</td>
                                            <td>
                                                <div className="d-flex">
                                                    <Link state={{ data: portofolio }} to={`/editportofolio/${portofolio._id}`} className="btn btn-warning ms-2">Edit</Link>
                                                    <button className="btn btn-danger ms-2" onClick={() => deletePortofolio(portofolio._id)}>Delete</button>
                                                </div>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    )
};
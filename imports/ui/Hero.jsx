import React from "react";
import { Link } from "react-router-dom";

export const Hero = () => (
    <div className="header">
        <nav className="navbar justify-content-between d-flex flex-row">
            <h3><b>Rawork.</b></h3>
            <div >
                <ul>
                    <li><Link to="#">Home</Link></li>
                    <li><Link to="#">About</Link></li>
                    <li><Link to="/testi">Testimonials</Link></li>
                    <li><Link to="#">Contact Us</Link></li>
                    {/* <li><Link to="/register">Sign Up</Link></li> */}
                    <li><Link to="/login" className="btn btn1">Sign In</Link></li>
                </ul>
            </div>
        </nav>
        <div className="hero">
            <div className="row">
                <div className="col-md-6">
                    <h1>Build digital <br /> product, brands <br /> experience.</h1>
                    <h6>a <b>Product Designer</b> and <b>Product Developer</b> in ID. <br /> 
                        we specialize in UI/UX Design, Responsive Web Design, <br />
                        and Mobile Developer</h6>
                    <Link to="/login" className="btn btn2">Get Started</Link>
                </div>
                <div className="col-md-6">
                    <img className="hero-img" src="/hero-img.png"/>
                </div>
            </div>
        </div>
    </div>
);
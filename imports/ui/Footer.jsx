import React from "react";

export const Footer = () => (
    <div>
        <footer>
            <div className="futer">
                <div className="container text-center text-md-left">
                    <div className="row">
                        <div className="col-md-4 mt-md-0 mt-3">
                            <h3><b>Rawork.</b></h3>
                            <p>Rawork is a <b>Product Designer</b> <br />
                            and <b>Product Developer</b> in ID.</p>
                            <div className="futer-icon">
                                <div className="row">
                                    <div className="col-md-12">
                                        <img src="./icon/ic_dribbble.png" />
                                        <img src="./icon/ic_github.png" />
                                        <img src="./icon/ic_linkedin.png" />
                                        <img src="./icon/ic_instagram.png" />
                                        <img src="./icon/ic_facebook.png" />
                                        <img src="./icon/ic_twitter.png" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr className="clearfix w-100 d-md-none pb-3" />
                        <div className="col-md-2 mb-md-0 mb-3">
                            <h5 className="text-uppercase text-start">Our Services</h5>
                            <ul className="list-unstyled">
                                <li>
                                    <a href="#">UI/UX Design</a>
                                </li>
                                <li>
                                    <a href="#">Web Design</a>
                                </li>
                                <li>
                                    <a href="#">Mobile Dev</a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-2 mb-md-0 mb-3">
                            <h5 className="text-uppercase text-start">About</h5>
                            <ul className="list-unstyled d-flex flex-column align-items-start">
                                <li>
                                    <a href="#">General Info</a>
                                </li>
                                <li>
                                    <a href="#">FAQ</a>
                                </li>
                                <li>
                                    <a href="#">Privacy Policy</a>
                                </li>
                                <li>
                                    <a href="#">Terms of Use</a>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-2 mb-md-0 mb-3">
                            <h5 className="text-uppercase text-start">Contact Us</h5>
                            <ul className="list-unstyled">
                                <li>
                                    <a href="#">0821-2345-6789</a>
                                </li>
                                <li>
                                    <a href="#">www.rawork.com</a>
                                </li>
                                <li>
                                    <a href="#">info@rawork.com</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
);
import React from 'react';
import { Hero } from './Hero.jsx';
import { Service } from './Service.jsx';
import { Team } from './Team.jsx';
import { Portofolio } from './Portofolio.jsx';
import { Testimoni } from './Testimoni.jsx';
import { Footer } from './Footer.jsx';


export const Landing = () => (
  <div>
    <Hero/>
    <Service/>
    <Team/>
    <Portofolio/>
    <Testimoni/>
    <Footer/>
  </div>
);

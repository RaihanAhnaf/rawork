import React from "react";

const PortoTile = (props) => {
    return(
        <div className="col-md-3">
            <div className="card box-shadow-hover">
                <center>
                    <img className="porto-img" src={props.portoImg}/>
                    <h4>{props.title}</h4>
                    <h6>{props.description}</h6>
                </center>
            </div>
        </div>
    );
}

export default PortoTile;
import React from "react";

export default function Services (props) {
    return(
        <div className="col-md-4">
            <div className="card box-shadow-hover">
                <center>
                    <img className="serv-img" src={props.imgUrl}/>
                    <h3>{props.title}</h3>
                    <h6 className="card-desc">{props.description}</h6>
                </center>
            </div>
        </div>
    );
}
import React from "react";

export default function Testi (props) {
    return(
        <div className="testi col-md-4">
            <div className="card box-shadow-hover">
                <h5>{props.name}</h5>
                <p>{props.review}</p>
            </div>
        </div>
    );
}
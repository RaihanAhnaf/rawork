import React from "react";

const TeamTile = (props) => {
    return(
        <div className="col-md-3">
            <div className="grid">
                <figure className="hvr-effect">
                    <div className="card">
                        <img src={props.foto}/>
                        <figcaption>
                            <h2>{props.nama}</h2>
                            <h6>{props.jabatan}</h6>
                        </figcaption>
                    </div>
                </figure>
            </div>
        </div>
    );
}

export default TeamTile;
import React from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Landing } from './Landing';
import { AdminTestimoni } from './AdminTestimoni';
import { AdminPortofolio } from './AdminPortofolio';
import { AddPortofolio } from './AddPortofolio';
import { EditPortofolio } from './EditPortofolio';
import { Login } from './Login';
import { Register } from './Register';
import { RecoilRoot } from 'recoil';
import Authenticated from './middleware/Authenticated';

export const App = () => {
  return (
    <RecoilRoot>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Landing />} />
          <Route path="/testi" element={<Authenticated> <AdminTestimoni /> </Authenticated>} />
          <Route path="/portofolio" element={<Authenticated> <AdminPortofolio /> </Authenticated>} />
          <Route path="/addportofolio" element={<Authenticated> <AddPortofolio /> </Authenticated>} />
          <Route path="/editportofolio/:id" element={<Authenticated> <EditPortofolio /> </Authenticated>} />
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
        </Routes>
      </BrowserRouter>
    </RecoilRoot>
  )
};

import React from "react";
import Testi from "./components/testi/Testi";
import { useTracker } from "meteor/react-meteor-data";
import { TestimoniCollection } from "../api/testimoni";
import { Link } from "react-router-dom";

export const Testimoni = () => {
    const testimoni = useTracker(() => TestimoniCollection.find({status: true}).fetch())
    const [testimonial, setTestimonal] = React.useState({name:"", review:""})
    const handleTestimoni = (e) => {
        e.preventDefault()
        Meteor.call("testimoni.insert", testimonial.name, testimonial.review)
    }
    return(
        <div>
        <div className="testi container">
            <center>
                <h2>Testimonials</h2>
            </center>
                <div className="row">
                    {
                        testimoni.map(testimoni => {
                            return <Testi 
                            name = {testimoni.name}
                            review = {testimoni.review}
                            />
                        })
                    }
                </div>
            <center>
                <h2>Your Testimonial is Helping Us</h2>
            </center>
                <div className="form-control form-control-inline">
                    <form onSubmit={handleTestimoni}>
                        <div className="col-md-12">
                        <div className="form-group">
                            <label>Full Name</label><br />
                            <input className="form-control" type="text" onChange={(e) => setTestimonal({...testimonial, name: e.target.value})} />
                            <br />
                        </div>
                        </div>
                        <div className="form-group">
                            <label>Your Review About Rawork.</label>
                            <br />
                            <textarea className="form-control" rows="3" onChange={(e) => setTestimonal({...testimonial, review: e.target.value})}></textarea>
                        </div>
                        <br />
                        <div className="d-grid gap-2 col-2 mx-auto">
                            <button className="btn btn3" type="submit">Submit</button>
                            <br />
                        </div>
                    </form>
                </div>
        </div>
        <div className="iklan justify-content-between d-flex flex-row">
            <h4>Build digital product, brands with Rawork. Now!</h4>
            <Link to="/login" className="btn btn1">Get Started</Link>
        </div>
    </div>
    )    
};
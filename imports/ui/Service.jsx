import React from "react";
import Services from "./components/service/Services";
import { useTracker } from "meteor/react-meteor-data";
import { ServicesCollection } from "../api/services";

export const Service = () => {
    const services = useTracker(() => ServicesCollection.find({}).fetch())
    return(
        <div>
        <div className="h_serv">
            <center><h2>What we offer</h2>
            <h6>Learn about our service</h6></center>
            <div className="container">
                <div className="row">
                    {
                        services.map(service => {
                            return <Services 
                            imgUrl = {service.img}
                            title = {service.title}
                            description = {service.description}
                            key = {service._id}
                            />
                        })
                    }                    
                </div>
            </div>
        </div>
    </div>
    )    
};
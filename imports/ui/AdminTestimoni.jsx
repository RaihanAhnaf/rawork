import React from "react";
import { useTracker } from "meteor/react-meteor-data";
import { TestimoniCollection } from "../api/testimoni";
import { Link } from "react-router-dom";
import { authenticated } from "./store";
import { useRecoilState } from "recoil";

export const AdminTestimoni = () => {
    const [auth, setAuth] = useRecoilState(authenticated);
    const testimoni = useTracker(() => TestimoniCollection.find({}).fetch())
    const deleteTesti = (id) => Meteor.call("testimoni.delete", id)
    const tampilTesti = (id, status) => Meteor.call("testimoni.tampil", id, status)
    const logout = () => {
        Meteor.logout((err) => {
          if (err) console.log(err);
          else {
            setAuth({ check: false, user: [] });
          }
        });
      };
    return (
        <>
            <div className="admtesti">
                <nav className="navbar justify-content-between d-flex flex-row">
                    <h3><b>Rawork.</b></h3>
                    <div >
                        <ul>
                            <li><Link to="/">Home</Link></li>
                            <li><Link to="/portofolio">Portofolio</Link></li>
                            <li><Link to="/testi">Testimoni</Link></li>
                            <li onClick={() => logout()}><Link to="#">Logout</Link></li>
                        </ul>
                    </div>
                </nav>
                <div className="container">
                    <center>
                        <h1>Testimonial</h1>
                    </center>
                    <table className="table table-hover">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Full Name</th>
                                <th>Review</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                testimoni.map((testimoni, index) => {
                                    return (
                                        <tr>
                                            <td>{index + 1}</td>
                                            <td>{testimoni.name}</td>
                                            <td>{testimoni.review}</td>
                                            <td>{testimoni.status ? <span class="badge rounded-pill bg-success">Tampil</span> : <span class="badge rounded-pill bg-danger">Sembunyikan</span>}</td>
                                            <td>
                                                <div className="d-flex">
                                                    {testimoni.status ? <button className="btn btn-warning" onClick={() => tampilTesti(testimoni._id, false)}>Sembunyikan</button> : <button className="btn btn-primary" onClick={() => tampilTesti(testimoni._id, true)}>Tampil</button>}
                                                    <button className="btn btn-danger ms-2" onClick={() => deleteTesti(testimoni._id)}>Delete</button>
                                                </div>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    )
};
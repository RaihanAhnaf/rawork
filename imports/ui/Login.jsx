import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { Meteor } from "meteor/meteor";
import { authenticated } from "./store";
import { useRecoilState } from "recoil";

export const Login = () => {
    let navigate = useNavigate();
    const [form, setForm] = React.useState({username: '', password: ''});
    const [auth, setAuth] = useRecoilState(authenticated);

    const handleLogin = (e) => {
        e.preventDefault();
        Meteor.loginWithPassword(form.username, form.password, (error) => {
            if (error) console.log(error);
            else {
                const user = Meteor.user();
                setAuth({check: true, user});
                navigate('/portofolio');
            }
        });
    }

    return (
        <div>
            <div className="login">
                <nav className="navbar justify-content-between d-flex flex-row">
                    <h3><b>Rawork.</b></h3>
                    <div >
                        <ul>
                            <li><Link to="/">Home</Link></li>
                            {/* <li><Link to="/register">Sign Up</Link></li> */}
                        </ul>
                    </div>
                </nav>
                <div className="container">
                    <center>
                        <h1>Sign In</h1>
                    </center>
                    <div className="form-control form-control-inline">
                        <form onSubmit={handleLogin}>
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Username</label><br />
                                    <input className="form-control" type="text" onChange={(e) => setForm({...form, username: e.target.value})}/>
                                <br />
                                </div>
                            </div>
                            <div className="form-group">
                                <label>Password</label><br />
                                <input className="form-control" type="password" onChange={(e) => setForm({...form, password: e.target.value})}/>
                              <br />
                            </div>
                            <br />
                            <div className="d-grid gap-2 col-2 mx-auto">
                                <button className="btn btn3" type="submit">Submit</button>
                                <br />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}
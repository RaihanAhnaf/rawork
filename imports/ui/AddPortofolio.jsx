import React from "react";
import { useTracker } from "meteor/react-meteor-data";
import { PortofolioCollection } from "../api/portofolio";
import { Link } from "react-router-dom";

export const AddPortofolio = () => {
    const portofolio = useTracker(() => PortofolioCollection.find({}).fetch())
    const [addportofolio, setPortofolio] = React.useState({ img: "", title: "", description: "" })
    const handlePortofolio = (e) => {
        e.preventDefault()
        Meteor.call("portofolio.insert", addportofolio.img, addportofolio.title, addportofolio.description)
    }
    const getBase64 = (file, cb) => {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result);
        };
        reader.onerror = function (error) {
            console.log("Error: ", error);
        };
    };
    const getImg = (e) => {
        getBase64(e.target.files[0], (res) => {setPortofolio({...addportofolio, img:res})})
    }

    return (
        <div>
            <div className="addPorto">
                <nav className="navbar justify-content-between d-flex flex-row">
                    <h3><b>Rawork.</b></h3>
                    <div >
                        <ul>
                            <li><Link to="/portofolio">Back</Link></li>
                        </ul>
                    </div>
                </nav>
                <div className="container">
                    <center>
                        <h1>Tambah Portofolio</h1>
                    </center>
                    <div className="form-control form-control-inline">
                        <form onSubmit={handlePortofolio}>
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Gambar Portofolio</label><br />
                                    <input className="form-control-file" type="file" onChange={getImg} />
                                    <br />
                                </div>
                                <div className="form-group">
                                    <label>Judul Portofolio</label><br />
                                    <input className="form-control" type="text" onChange={(e) => setPortofolio({ ...addportofolio, title: e.target.value })} />
                                    <br />
                                </div>
                            </div>
                            <div className="form-group">
                                <label>Description</label>
                                <br />
                                <input className="form-control" type="text" onChange={(e) => setPortofolio({ ...addportofolio, description: e.target.value })} />
                            </div>
                            <br />
                            <div className="d-grid gap-2 col-2 mx-auto">
                                <button className="btn btn3" type="submit">Submit</button>
                                <br />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}
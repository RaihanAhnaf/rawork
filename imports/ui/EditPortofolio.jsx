import React, { useEffect } from "react";
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import { useTracker } from "meteor/react-meteor-data";
import { PortofolioCollection } from "../api/portofolio";

export const EditPortofolio = () => {
    let {id} = useParams();
    const location = useLocation()
    let navigate = useNavigate()
    const portofolio = PortofolioCollection.findOne({_id: id});
    const [addportofolio, setPortofolio] = React.useState({ img: "", title: "", description: "" })
    const handlePortofolio = (e) => {
        e.preventDefault()
        Meteor.call("portofolio.update", portofolio._id, addportofolio.img, addportofolio.title, addportofolio.description, (error) => {
            if (error) console.log(error)
            else navigate("/portofolio")
        })
    }
    console.log(portofolio)
    useEffect(() => {
        setPortofolio({ ...addportofolio, img: "", title: portofolio.title, description: portofolio.description })
    }, [])
    const getBase64 = (file, cb) => {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result);
        };
        reader.onerror = function (error) {
            console.log("Error: ", error);
        };
    };
    const getImg = (e) => {
        getBase64(e.target.files[0], (res) => { setPortofolio({ ...addportofolio, img: res }) })
    }

    return (
        <div>
            <div className="editPorto">
                <nav className="navbar justify-content-between d-flex flex-row">
                    <h3><b>Rawork.</b></h3>
                    <div >
                        <ul>
                            <li><Link to="/portofolio">Back</Link></li>
                        </ul>
                    </div>
                </nav>
                <div className="container">
                    <center>
                        <h2>Edit Portofolio</h2>
                    </center>
                    <div className="form-control form-control-inline">
                        <form onSubmit={handlePortofolio}>
                            <div className="col-md-12">
                                <div className="form-group">
                                    <label>Gambar Portofolio</label><br />
                                    <input className="form-control-file" type="file" onChange={getImg} />
                                    <br />
                                </div>
                                <div className="form-group">
                                    <label>Judul Portofolio</label><br />
                                    <input value={addportofolio.title} className="form-control" type="text" onChange={(e) => setPortofolio({ ...addportofolio, title: e.target.value })} />
                                    <br />
                                </div>
                            </div>
                            <div className="form-group">
                                <label>Description</label>
                                <br />
                                <input value={addportofolio.description} className="form-control" type="text" onChange={(e) => setPortofolio({ ...addportofolio, description: e.target.value })} />
                            </div>
                            <br />
                            <div className="d-grid gap-2 col-2 mx-auto">
                                <button className="btn btn3" type="submit">Submit</button>
                                <br />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}
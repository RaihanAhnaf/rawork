import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { PortofolioCollection } from '../imports/api/portofolio';
import { ServicesCollection } from '../imports/api/services';
import { TestimoniCollection } from '../imports/api/testimoni';
import '../imports/api/testimoialMethod';
import '../imports/api/portofolioMethod';
import '../imports/api/portofolioPublications';

const SEED_NAME = "Admin Raihan";
const SEED_USERNAME = 'admin';
const SEED_PASSWORD = 'admin123';

function insertServices({ img, title, description }) {
  ServicesCollection.insert({img, title, description, createdAt: new Date()});
}

function insertPortofolio({img, title, description}) {
  PortofolioCollection.insert({img, title, description, createdAt: new Date()});
}

function insertTestimoni({name, review}) {
  TestimoniCollection.insert({name, review, status:false, createdAt: new Date()});
}

Meteor.startup(() => {
  if (!Accounts.findUserByUsername(SEED_USERNAME)) {
    Accounts.createUser({
      username: SEED_USERNAME,
      password: SEED_PASSWORD,
      profile: {
        name: SEED_NAME
      }
    });
  }

  if (ServicesCollection.find().count() === 0) {
    insertServices({
      img: 'uiux.png',
      title: 'UI/UX Designs',
      description: 'lorem'
    });

    insertServices({
      img: 'webdesign.png',
      title: 'Web Design',
      description: 'lorem'
    });

    insertServices({
      img: 'mobiledeveloper.png',
      title: 'Mobile Developer',
      description: 'ipsum'
    });
  }

  if (PortofolioCollection.find().count() === 0) {
    insertPortofolio({
      img: 'card1.png',
      title: 'Portofolio #1',
      description: 'UI Design'
    });

    insertPortofolio({
      img: 'card1.png',
      title: 'Portofolio #2',
      description: 'UI Design'
    });

    insertPortofolio({
      img: 'card1.png',
      title: 'Portofolio #3',
      description: 'UI Design'
    });

    insertPortofolio({
      img: 'card1.png',
      title: 'Portofolio #4',
      description: 'UI Design'
    });
  }

  if (TestimoniCollection.find().count() === 0) {
    insertTestimoni({
      name: 'Budi Santoso',
      review: 'Wahhhh sangat bagus sekali Rawork ini!!',
    });
  }
});
